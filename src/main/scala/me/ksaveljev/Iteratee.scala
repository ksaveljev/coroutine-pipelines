package me.ksaveljev

import scalaz.{Applicative, Functor, Monad, MonadTrans}

/*
 * We can easily define a monad transformer dual to generators, whose
 * suspension demands a value instead of yielding. The terminology for
 * this kind of coroutine is not as well established as for generators,
 * but in the Haskell community the name iteratee appears to be the
 * most popular
 */
case class Iteratee[A, M[_], X](bounceIter: M[Either[A => Iteratee[A, M, X], X]])

object Iteratee {

  implicit class IterateeOps[A, M[_], X](g: Iteratee[A, M, X])(implicit ev: Functor[M]) {
    def map[Y](f: X => Y): Iteratee[A, M, Y] = iterateeFunctor[A, M].map(g)(f)
  }

  implicit def iterateeFunctor[A, M[_]](implicit M: Functor[M]): Functor[Iteratee[A, M, ?]] = new Functor[Iteratee[A, M, ?]] {
    def map[B, C](fb: Iteratee[A, M, B])(f: B => C): Iteratee[A, M, C] = {
      ??? // TODO
    }
  }

  implicit def iterateeApplicative[A, M[_]](implicit M: Applicative[M]): Applicative[Iteratee[A, M, ?]] = new Applicative[Iteratee[A, M, ?]] {
    def point[B](b: => B): Iteratee[A, M, B] = ??? // TODO
    def ap[B, C](fb: => Iteratee[A, M, B])(f: => Iteratee[A, M, B => C]): Iteratee[A, M, C] = {
      ??? // TODO
    }
  }

  implicit def iterateeMonad[A, M[_]](implicit M: Monad[M]): Monad[Iteratee[A, M, ?]] = new Monad[Iteratee[A, M, ?]] {
    def point[B](b: => B): Iteratee[A, M, B] = ??? // TODO
    def bind[B, C](fb: Iteratee[A, M, B])(f: B => Iteratee[A, M, C]): Iteratee[A, M, C] = {
      ??? // TODO
    }
  }

  implicit def iterateeMonadTrans[A]: MonadTrans[Iteratee[A, ?[_], ?]] = new MonadTrans[Iteratee[A, ?[_], ?]] {
    def liftM[G[_], B](b: G[B])(implicit G: Monad[G]): Iteratee[A, G, B] = ??? // TODO
    implicit def apply[G[_]](implicit G: Monad[G]): Monad[Iteratee[A, G, ?]] = iterateeMonad[A, G]
  }

  def await[A, M[_]](implicit M: Monad[M]): Iteratee[A, M, A] =
    Iteratee(M.point(Left((a: A) => iterateeMonad.point(a))))

  // To run a monad thus transformed, we have to supply it with values
  def runIteratee[A, M[_], X](input: List[A])(it: Iteratee[A, M, X])(implicit M: Monad[M]): M[X] = {
    input match {
      case a :: rest => M.bind(it.bounceIter) {
        case Left(t)  => runIteratee(rest)(t(a))
        case Right(v) => M.point(v)
      }
      case Nil       => M.bind(it.bounceIter) {
        case Left(t)  => runIteratee(List[A]())(t(sys.error("No more values to feed.")))
        case Right(v) => M.point(v)
      }
    }
  }
}
