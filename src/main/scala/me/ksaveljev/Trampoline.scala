package me.ksaveljev

import scalaz.{Functor, Applicative, Monad, MonadTrans}

/*
 * Trampoline is a simple monad transformer that allows the base monad to pause
 * its computation at any time
 *
 * Once lifted on a Trampoline, in a manner of speaking, a computation from the
 * base monad becomes a series of alternating bounces and pauses. The bounces
 * are the uninterruptible steps in the base monad, and the during the pauses
 * the trampoline turns control over to us
 */
case class Trampoline[M[_], R](bounce: M[Either[Trampoline[M, R], R]])

object Trampoline {

  implicit class TrampolineOps[A, M[_]](t: Trampoline[M, A])(implicit ev: Functor[M]) {
    def map[B](f: A => B): Trampoline[M, B] = trampolineFunctor[M].map(t)(f)
  }

  implicit def trampolineFunctor[M[_]](implicit M: Functor[M]): Functor[Trampoline[M, ?]] = new Functor[Trampoline[M, ?]] {
    def map[A, B](fa: Trampoline[M, A])(f: A => B): Trampoline[M, B] = {
      ??? // TODO
    }
  }

  implicit def trampolineApplicative[M[_]](implicit M: Applicative[M]): Applicative[Trampoline[M, ?]] = new Applicative[Trampoline[M, ?]] {
    def point[A](a: => A): Trampoline[M, A] = ??? // TODO
    def ap[A, B](fa: => Trampoline[M, A])(f: => Trampoline[M, A => B]): Trampoline[M, B] = {
      ??? // TODO
    }
  }

  implicit def trampolineMonad[M[_]](implicit M: Monad[M]): Monad[Trampoline[M, ?]] = new Monad[Trampoline[M, ?]] {
    def point[A](a: => A): Trampoline[M, A] = ??? // TODO
    def bind[A, B](fa: Trampoline[M, A])(f: A => Trampoline[M, B]): Trampoline[M, B] = {
      ??? // TODO
    }
  }

  implicit def trampolineMonadTrans: MonadTrans[Trampoline] = new MonadTrans[Trampoline] {
    def liftM[G[_], A](a: G[A])(implicit G: Monad[G]): Trampoline[G, A] = ??? // TODO
    implicit def apply[G[_]](implicit G: Monad[G]): Monad[Trampoline[G, ?]] = trampolineMonad[G]
  }

  def pause[M[_]](implicit M: Monad[M]): Trampoline[M, Unit] =
    Trampoline(M.point(Left(trampolineMonad[M].point(()))))

  /*
   * Function run can be used to eliminate all the pauses and restore
   * the original, un-lifted computation
   */
  def run[M[_], R](t: Trampoline[M, R])(implicit M: Monad[M]): M[R] = {
    M.bind(t.bounce)((e: Either[Trampoline[M, R], R]) => e match {
      case Left(tt) => run(tt)
      case Right(v) => M.point(v)
    })
  }
}
