package me.ksaveljev

import scalaz.{Applicative, Functor, Monad, MonadTrans}

/*
 * A limited form of coroutine called a Generator has become a part of JavaScript,
 * Python, and Ruby, among other recent programming languages. A generator is just
 * like a regular trampoline except it yields a value whenever it suspends.
 *
 * A generator is thus a coroutine whose every suspension provides not only the
 * coroutine resumption but also a single value
 */
case class Generator[A, M[_], X](bounceGen: M[Either[(A, Generator[A, M, X]), X]])

object Generator {

  implicit class GeneratorOps[A, M[_], X](g: Generator[A, M, X])(implicit ev: Functor[M]) {
    def map[Y](f: X => Y): Generator[A, M, Y] = generatorFunctor[A, M].map(g)(f)
  }

  implicit def generatorFunctor[A, M[_]](implicit M: Functor[M]): Functor[Generator[A, M, ?]] = new Functor[Generator[A, M, ?]] {
    def map[B, C](fb: Generator[A, M, B])(f: B => C): Generator[A, M, C] = {
      ??? // TODO
    }
  }

  implicit def generatorApplicative[A, M[_]](implicit M: Applicative[M]): Applicative[Generator[A, M, ?]] = new Applicative[Generator[A, M, ?]] {
    def point[B](b: => B): Generator[A, M, B] = ??? // TODO
    def ap[B, C](fb: => Generator[A, M, B])(f: => Generator[A, M, B => C]): Generator[A, M, C] = {
      ??? // TODO
    }
  }

  implicit def generatorMonad[A, M[_]](implicit M: Monad[M]): Monad[Generator[A, M, ?]] = new Monad[Generator[A, M, ?]] {
    def point[B](b: => B): Generator[A, M, B] = ??? // TODO
    def bind[B, C](fb: Generator[A, M, B])(f: B => Generator[A, M, C]): Generator[A, M, C] = {
      ??? // TODO
    }
  }

  implicit def generatorMonadTrans[A]: MonadTrans[Generator[A, ?[_], ?]] = new MonadTrans[Generator[A, ?[_], ?]] {
    def liftM[G[_], B](b: G[B])(implicit G: Monad[G]): Generator[A, G, B] = ??? // TODO
    implicit def apply[G[_]](implicit G: Monad[G]): Monad[Generator[A, G, ?]] = generatorMonad[A, G]
  }

  // yield a value whenever the computation suspends
  def yyield[A, M[_]](a: A)(implicit M: Monad[M]): Generator[A, M, Unit] =
    Generator(M.point(Left((a, generatorMonad[A, M].point(())))))

  /*
   * The difference between the old function run, that we’ve used for running a
   * Trampoline, and the new function runGenerator is that the latter collects
   * all values that its generator argument yields
   */
  def runGenerator[A, M[_], X](g: Generator[A, M, X])(implicit M: Monad[M]): M[(List[A], X)] = {
    def run(f: List[A] => List[A])(gg: Generator[A, M, X]): M[(List[A], X)] = {
      M.bind(gg.bounceGen) {
        case Left(t)  => run(f compose (t._1 :: _))(t._2)
        case Right(x) => M.point((f(List[A]()), x))
      }
    }

    run(identity)(g)
  }

}
