package me.ksaveljev

import me.ksaveljev.Trampoline._

import scalaz.effect.IO
import scalaz.syntax.monad._

object MainTrampoline extends App {

  val hello: Trampoline[IO, Unit] = for {
    _ <- IO.putStr("Hello ").liftM[Trampoline[?[_], ?]]
    _ <- pause[IO]
    _ <- IO.putStrLn("World!").liftM[Trampoline[?[_], ?]]
  } yield ()

  // we can just bounce the computation once, use the pause to perform
  // some other work, and then continue the trampoline
  val Left(continuation) = hello.bounce.unsafePerformIO()
  print("wonderful ")
  run(continuation).unsafePerformIO()
  // OUTPUT: Hello wonderful World!


  // run can be used to eliminate all the pauses and restore the
  // original, un-lifted computation
  run(hello).unsafePerformIO()
  // OUTPUT: Hello World!
}
