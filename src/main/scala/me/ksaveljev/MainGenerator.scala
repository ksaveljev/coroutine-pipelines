package me.ksaveljev

import me.ksaveljev.Generator._

import scalaz.effect.IO
import scalaz.syntax.monad._

object MainGenerator extends App {

  val gen: Generator[Int, IO, Int] = for {
    _ <- IO.putStr("Yielding one, ").liftM[Generator[Int, ?[_], ?]]
    _ <- yyield[Int, IO](1)
    _ <- IO.putStr("then two, ").liftM[Generator[Int, ?[_], ?]]
    _ <- yyield[Int, IO](2)
    _ <- IO.putStr("returning three: ").liftM[Generator[Int, ?[_], ?]]
  } yield 3

  val result = runGenerator(gen).unsafePerformIO()
  println(result)
  // OUTPUT: Yielding one, then two, returning three: ([1,2],3)

}
