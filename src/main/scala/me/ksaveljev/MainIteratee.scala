package me.ksaveljev

import me.ksaveljev.Iteratee._

import scalaz.effect.IO
import scalaz.syntax.monad._

object MainIteratee extends App {

  val it: Iteratee[Int, IO, Unit] = for {
    _ <- IO.putStr("Enter two numbers: ").liftM[Iteratee[Int, ?[_], ?]]
    a <- await[Int, IO]
    b <- await[Int, IO]
    _ <- IO.putStrLn(s"sum is ${a + b}").liftM[Iteratee[Int, ?[_], ?]]
  } yield ()

  runIteratee(List(4, 5))(it).unsafePerformIO()
  // OUTPUT: Enter two numbers: sum is 9
}
