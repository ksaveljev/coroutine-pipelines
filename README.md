### Progression

The following progression of modules is recommended (this list will be updated when additional exercises are ported from Haskell):

* `Trampoline`
* `Generator`
* `Iteratee`

### Verifying your solutions

There are no tests to accomodate the exercises. In order to check whether your solution works you can use one of the main entries which have a small example of usage of the concept:

* `MainTrampoline`
* `MainGenerator`
* `MainIteratee`
